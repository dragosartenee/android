package com.example.probleme3
enum class Colors (val nume: String, val valuare: Int) {
    Rosu(nume = "Rosu" , valuare = 10),
    Galben(nume="Galben" , valuare=15),
    Albastru(nume="Albastru", valuare=20),
}

fun main(){
    val colors= enumValues<Culori>().forEach { println(it.name) }
    //val veg1= Vegetables("castravete",1)
    try {
        println("introdu nr")
        var numar:Int=readLine()!!.toInt()
        var impartire=100/numar
    }catch(ex:ExceptionInInitializerError){println(ex.message)}
    val numberList = mutableListOf<Double>()
    numberList.also { println("Adaugare lista") }
        .apply {
            add(2.71)
            add(3.14)
            add(1.0)
        }
        .also { println("$numberList") }
}
//daca se apasa 0 programul intra in try catch daca se apsa alta cifra va aparea si numberList
